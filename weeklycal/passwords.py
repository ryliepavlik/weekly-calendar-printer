# Copyright 2021, Collabora, Ltd.
#
# SPDX-License-Identifier: MIT
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>

from typing import Dict, Optional, Tuple


try:
    import secretstorage

    HAVE_SECRETSTORAGE = True
except ImportError:
    HAVE_SECRETSTORAGE = False


def _make_attrs(server: str, username: str) -> Tuple[str, Dict[str, str]]:
    return f"WeeklyCalendar-{username}", {
        "application": "weekly-calendar",
        "server": server,
        "username": username,
    }


def get_password(server: str, username: str) -> Optional[str]:
    if HAVE_SECRETSTORAGE:
        connection = secretstorage.dbus_init()
        if not secretstorage.check_service_availability(connection):
            return None
        collection = secretstorage.get_default_collection(connection)
        _, attrs = _make_attrs(server, username)
        result_iter = collection.search_items(attrs)
        result: Optional[secretstorage.Item] = next(result_iter, None)
        if result is None:
            return None
        if result.is_locked():
            if result.unlock():
                # prompt was dismissed
                return None
        return result.get_secret().decode("utf-8")
    return None


def store_password(server: str, username: str, password: str) -> bool:
    if HAVE_SECRETSTORAGE:
        connection = secretstorage.dbus_init()
        if not secretstorage.check_service_availability(connection):
            return False
        collection = secretstorage.get_default_collection(connection)
        label, attrs = _make_attrs(server, username)
        collection.create_item(label, attrs, password.encode("utf-8"))
        return True
    return False
