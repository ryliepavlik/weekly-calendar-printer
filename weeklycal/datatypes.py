# Copyright 2021, Collabora, Ltd.
#
# SPDX-License-Identifier: MIT
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>

from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Optional

from dateutil import tz


@dataclass
class CalendarEvent:
    """Just the data we need."""

    summary: str
    start: datetime
    duration: Optional[timedelta] = None

    def set_duration_from_end(self, end: datetime):
        self.duration = end.astimezone(tz=tz.tzlocal()) - self.start.astimezone(
            tz=tz.tzlocal()
        )
