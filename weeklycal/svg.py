# Copyright 2021-2022, Collabora, Ltd.
#
# SPDX-License-Identifier: MIT
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>

from typing import List, Tuple, cast
from copy import deepcopy

import pkg_resources
import svgutils.transform as sg
from lxml import etree
import datetime
from dataclasses import dataclass

_HOURS = list(range(9, 19))


_NSMAP = {
    "svg": sg.SVG_NAMESPACE,
}

_WEEKDAYS = ("monday", "tuesday", "wednesday", "thursday", "friday")

_DAYS_OF_THE_WEEK_XPATH = [
    etree.XPath('//*[@id="date_%s"]' % weekday, namespaces=_NSMAP)
    for weekday in _WEEKDAYS
]

_DAYS_OF_THE_WEEK_GROUPS_XPATH = [
    etree.XPath('//*[@id="group_%s"]' % weekday, namespaces=_NSMAP)
    for weekday in _WEEKDAYS
]


def _get_week_svg_string():
    return pkg_resources.resource_string(__name__, "week.svg").decode(encoding="utf-8")


def _replace_in_style(elt, old, new):
    elt.set("style", elt.get("style", "").replace(old, new))


@dataclass
class CalendarSvg:
    figure: sg.SVGFigure
    weekday_dates: List
    date_groups: Tuple[etree.ElementBase, ...]
    hour_height: float

    month_tspan: etree.ElementBase
    event_template: etree.ElementBase

    # classification_tspan: etree.ElementBase

    monday: datetime.date

    current_event_id: int = 0

    def _compute_y_location(self, hour: int, minute: int):
        hour_index = hour - _HOURS[0]
        decimal_hours = hour_index + minute / 60.0
        return decimal_hours * self.hour_height

    def _compute_box_y_range(self, start: datetime.time, duration: datetime.timedelta):
        """Get the start y position and y length for an event"""
        total_hours = duration.total_seconds() / (60.0 * 60.0)
        return (
            self._compute_y_location(start.hour, start.minute),
            total_hours * self.hour_height,
        )

    def make_event(
        self,
        start: datetime.time,
        duration: datetime.timedelta,
        message: str,
    ) -> etree.ElementBase:
        event = deepcopy(self.event_template)
        # make visible
        event.set("style", "display:inline;")
        # make ID unique
        id_str = f"event_{self.current_event_id}"
        self.current_event_id += 1

        event.set("id", id_str)
        start_y, y_length = self._compute_box_y_range(start, duration)
        event.set("transform", f"translate(0,{start_y})")

        text_elt = event.find("svg:text", namespaces=_NSMAP)
        rect_elt = event.find("svg:rect", namespaces=_NSMAP)
        old_rect_id = rect_elt.get("id")
        rect_id = f"rect_{id_str}"

        rect_elt.set("id", rect_id)
        rect_elt.set("height", str(y_length))

        text_elt.text = ""
        outer_tspan_elt = text_elt.find("svg:tspan", namespaces=_NSMAP)
        outer_tspan_elt.text = ""
        tspan_elt = outer_tspan_elt.find("svg:tspan", namespaces=_NSMAP)
        tspan_elt.text = message
        _replace_in_style(tspan_elt, old_rect_id, rect_id)
        _replace_in_style(text_elt, old_rect_id, rect_id)
        text_elt.set("id", f"text_{id_str}")

        return event

    def add_event(
        self,
        start: datetime.datetime,
        duration: datetime.timedelta,
        message: str,
    ):
        weekday = start.weekday()
        start_time = start.time()
        event = self.make_event(start_time, duration, message)
        if weekday >= len(self.date_groups):
            print("Skipping weekend event", message, start)
            return
        date_group = self.date_groups[weekday]
        date_group.append(event)

    def create_new_including_this(self) -> sg.SVGFigure:
        svg = sg.SVGFigure()
        svg.set_size((f"{self.figure.width}px", f"{self.figure.height}px"))
        svg.append(self.figure)

        return svg

    def set_dates(self, dates: Tuple[int, int, int, int, int]):
        for elt, date in zip(self.weekday_dates, dates):
            elt.text = str(date)
        self.month_tspan.text = self.monday.strftime("%B")

    @classmethod
    def load(cls, start_date: datetime.date, classification: str = "Internal"):
        assert start_date.weekday() == 0
        day_nums = [
            (start_date + datetime.timedelta(days=offset)).day for offset in range(5)
        ]

        svg_str = _get_week_svg_string()
        bg_svg = sg.fromstring(svg_str)
        etree.fromstring(svg_str.encode(), parser=etree.XMLParser(huge_tree=True))

        weekday_date_text: List[etree.ElementBase] = [
            query(bg_svg.root)[0] for query in _DAYS_OF_THE_WEEK_XPATH
        ]
        weekday_dates: List[etree.ElementBase] = [
            text.find("svg:tspan", namespaces=_NSMAP) for text in weekday_date_text
        ]
        date_groups = [
            query(bg_svg.root)[0] for query in _DAYS_OF_THE_WEEK_GROUPS_XPATH
        ]
        example_date_rect: etree.ElementBase = date_groups[0].find(
            "svg:rect", namespaces=_NSMAP
        )

        month_tspan = bg_svg.root.xpath(
            '//*[@id="month"]/svg:tspan', namespaces=_NSMAP
        )[0]

        date_height = float(example_date_rect.get("height", None))
        hour_height = float(date_height) / len(_HOURS)
        template = bg_svg.root.xpath('//*[@id="template"]')[0]

        classification_tspan = bg_svg.root.xpath(
            '//svg:tspan[@id="classification"]', namespaces=_NSMAP
        )[0]
        classification_tspan.text = classification

        # pyright: reportGeneralTypeIssues=false
        result = CalendarSvg(
            figure=bg_svg,
            weekday_dates=weekday_dates,
            date_groups=date_groups,
            hour_height=hour_height,
            event_template=template,
            monday=start_date,
            month_tspan=month_tspan,
            # classification_tspan=classification_tspan,
        )
        result.set_dates(cast(Tuple[int, int, int, int, int], tuple(day_nums)))
        return result
