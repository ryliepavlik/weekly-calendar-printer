# Weekly Calendar

<!--
    SPDX-FileCopyrightText: 2021, Collabora, Ltd.
    SPDX-License-Identifier: CC-BY-4.0

    Author: Rylie Pavlik <rylie.pavlik@collabora.com>
-->

This is a tool to generate a printable SVG containing a week's schedule. The
default template differs from other schedule printouts in that it allows
additional space to the right of any scheduled events, to write in what actually
happened, for use in timelogging.

You will want to have the [Noto Sans](https://www.google.com/get/noto/) font
family installed, as it is used in the bundled SVG template. (You could modify
the template to use a different font: just be sure to keep the elements in the
SVG that have human-sounding ID attributes, as the script uses them.) You will
also want [Inkscape](https://inkscape.org) for printing the generated SVG.

If using on Linux, installing
[`SecretStorage`](https://pypi.org/project/SecretStorage/) is strongly
recommended, so that your password may be saved securely.

## Setup

No need to install system-wide. Requires Python 3.7+.

```sh
# Set up a virtual environment and install into it
python3 -m venv venv
. venv/bin/activate   # or equivalent for your chosen shell
python3 -m pip install --editable .

# recommended on Linux:
python3 -m pip install secretstorage
```

## Usage

```sh
# To run, be sure to active the virtual environment
. venv/bin/activate   # or equivalent for your chosen shell

# then run the tool
weekly-calendar --help   # for help

weekly-calendar thisweek.svg  # common usage if you already saved credentials, etc.
```

After installing the venv, you may also use the created launcher scripts inside
the venv without activating it, due to the usage of setuptools.

You might consider putting your server and username in a `.env` file. Type the
password when prompted, if you have `SecretStorage` installed and working, it
will be saved at the end of running.

## License

This repo complies with [REUSE](https://reuse.software): the copyright and
license are clearly marked in each file. The code is all MIT, the art assets are
CC0-1.0 for clarity. This README is CC-BY-4.0. For a software bill of materials,
run `reuse spdx`.
