#!/usr/bin/env python3
# Copyright (c) 2021-2024 Collabora, Ltd.
#
# SPDX-License-Identifier: MIT
"""Setup."""

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Weekly Calendar",
    version="0.2",
    author="Rylie Pavlik",
    author_email="rylie.pavlik@collabora.com",
    license="MIT",
    packages=setuptools.find_packages(),
    install_requires=[
        "svgutils>=0.3.4,<1",
        "click>=7,<9",
        "python-dotenv>=0.17.1,<1",
        "caldav>=0.7",
        "vobject",
        "pytz",
    ],
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "weekly-calendar=weeklycal.main:cli",
            "weekly-calendar-blank=weeklycal.blank:cli",
        ]
    },
    python_requires=">=3.7",
)
