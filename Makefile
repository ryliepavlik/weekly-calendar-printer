#!/usr/bin/env make
# Copyright 2021, Collabora, Ltd.
# SPDX-License-Identifier: MIT

# Makefile to convert the "source" SVG into the simplified one included in the package.

INKSCAPE ?= inkscape

weeklycal/week.svg: art/week.src.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<
