# Weekly Calendar Printer License

<!--
    SPDX-FileCopyrightText: 2021, Collabora, Ltd.
    SPDX-License-Identifier: CC-BY-4.0

    Author: Rylie Pavlik <rylie.pavlik@collabora.com>
-->
This repo complies with [REUSE](https://reuse.software): the copyright and
license are clearly marked in each file. The code is all MIT, the art assets are
CC0-1.0 for clarity. This documentation file is CC-BY-4.0. For a software bill
of materials, run `reuse spdx`.

Full text of all licenses related to this software are in
[LICENSES](Licenses/).
